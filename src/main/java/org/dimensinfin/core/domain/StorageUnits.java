package org.dimensinfin.core.domain;

public class StorageUnits {
	public static long MEGABYTES = 1024 * 1024;
	public static long GIGABYTES = MEGABYTES * 1024;
}
